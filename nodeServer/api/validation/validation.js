/**
 * This file contains all the validation functions to validate request and response
 * 
 */
const config = require('../config/config');

exports.checkRequired = function(reqParams,apiName){ //check if required params exists in API
    if(Object.keys(reqParams).length < 1) return false;
    let error = 0
    config.api[apiName].required.forEach(element => {
        if(!reqParams[element]) {
            error = 1 
        }
    });
    return error ? false : true
}

exports.checkAllowed = function(reqParams,apiName){ //checks if user is pass only allowed Params
    if(Object.keys(reqParams).length < 1) return false;
    for(let key in reqParams){
        if(!config.api[apiName].allowedReqParams.hasOwnProperty(key)){
            return false
        }
    } 
    return true;
}

exports.filterData = function(data,apiName){ //filters response data from third party API calls
    if(Object.keys(data).length < 1) return false;
    var filteredRes = {}
    config.api[apiName].allowedResParams.forEach(element => {
        filteredRes[element] = data[element]
    });
    filteredRes['statuscode'] = data['cod']
    return filteredRes;
}
