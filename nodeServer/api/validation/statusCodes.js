/**
 * THIS FILE KEEPS THE LIST OF ALL STATUS CODES FOR  API:
 * 
 */

const statusCodes = {
    '401':'Unauthorized',
    '400':'Invalid request',
    '403':'Forbidden',
    '404':'Not Found',
    '200':'Sucess',
    '500': 'Interval server error'
}

module.exports = statusCodes;
