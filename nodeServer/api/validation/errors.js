/**
 * GENERIC ERROR FILE TO KEEP ALL THE DEBUGGING RELATED FUNCTIONS:
 * 
 */
//IMPORT ALL MODULES:
const statusCodes = require('./statusCodes');

exports.e = function(code,msg){ //e function to add user friendly error response
    return {'statuscode':code,'message': statusCodes[code]+(msg ? ': '+msg : '')}
}