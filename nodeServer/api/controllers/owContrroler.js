/**
 * Open weather API controller
 * 
 * 
 */
//IMPORT ALL MODULES:
const {e} = require('../validation/errors')
const { checkRequired,checkAllowed,filterData } = require('../validation/validation')
const {getApiResponse} = require('../services/owApi')

//CONTROLER FUNCTIONS:
//getResults gets the results from 
exports.getResults = async function(query, res) {
    
    if(!checkRequired(query,'ow')) return res.json(e(400,'required attributes missing')) //throw error if required params arenoe used
    else if(!checkAllowed(query,'ow')) return res.json(e(400,'please use only allowed attributes')) //only pass allowed params

    await getApiResponse(query,res) //call to api model to get the responce
        .then((response) => {
            if(response.cod=='200'){
                res.json(filterData(response,'ow')) //filter response data to clear not supported objects
            }else res.json(e(response.cod,response.message))
        })    
};

