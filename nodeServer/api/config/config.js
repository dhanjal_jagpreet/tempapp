/*
    Config file for api. This defines all the access info about api
*/
const config = {
    'api': {
            'ow':{
                'name':'Open Weather', //api name
                'key':'6ad18574570394e3b64cbb88fab86194', //api key
                'url':'https://api.openweathermap.org/data/2.5/weather', //api base url
                'allowedReqParams': {'city':'string','time':'string'}, //allowed params in api request
                'allowedResParams' : ['main','weather','name','timezone','sys'], //allowed response objects from API
                'required':['city'], //required params to run API
                'defaults': {'units' :'metric'} //default params of API
            },
            //add more apis here
    }
}

module.exports = config



