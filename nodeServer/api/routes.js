/**
 * Main routes file for API
 */

//IMPORT ALL MODULES:
const {app,express} = require('../app');
const owRes = require('../api/controllers/owContrroler');
const cors = require('cors');


//SETUP MIDDLEWARE:
app.use(cors());
app.use(express.json()) // handle json data
app.use(express.urlencoded({ extended: true })) // handle URL-encoded data

//INDEX ROUTE:
app.get('/', function(req,res) {
   res.send('Hi, Please use dedicated URL to access API')
});

//HANDLE REQUEST FROM BROWSER:
app.get('/api/ow/get', function(req,res,next){
   owRes.getResults(req.query,res)
});

//HANDLES POST REQUEST FROM CLIENT APPLICATION:
app.post('/api/ow/get', function(req,res){
    owRes.getResults(req.body,res)
    
   
});

//FOR ALL OTHER ROUTES:
app.all('*', function(req,res) {
    res.status(404).send('Error: Not Found')
});

module.exports = app;