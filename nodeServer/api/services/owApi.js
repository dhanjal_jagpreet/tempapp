/**
 * Open weather API Model
 * 
 */
//IMPORT ALL MODULES:
const axios = require('axios');
const config = require('../config/config')['api']['ow']
const {e} = require('../validation/errors')

//function to fetch the response from open weather API
exports.getApiResponse = function(queryParams,res) {
    let url = `${config.url}?q=${queryParams.city}&appid=${config.key}` //create URL
    
    for(let key in config['defaults']) //add default params for API call
        url += `&${key}=${config['defaults'][key]}`
    
    return  axios({ //fetch API data using axios which return promise to controller
                url: url,
                method: "get",
            })
            .then(response => {
                return response.data;
            })
            .catch((err) => {
                return err.response.data; 
            });
};
