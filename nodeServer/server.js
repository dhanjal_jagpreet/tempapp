var {app} = require('./app');
require('./api/routes');

var port = process.env.PORT || 3000; //set up local port

app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});

