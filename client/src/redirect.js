/**
 * set all the calls point to server API
 * 
 */
import axios from "axios";
export default axios.create({
  baseURL: "http://localhost:3000/api/ow",
});